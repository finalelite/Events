package br.com.finalelite.events.mini;

import br.com.finalelite.events.base.*;
import lombok.Getter;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public abstract class BaseMiniEvent<T> extends BaseEvent {

    @Getter
    private final Material icon;

    public BaseMiniEvent(JavaPlugin plugin, String name, String description, Material icon, EventReward reward,
                         long maxTimeInSeconds, EventItemPolicy itemPolicy, EventPlayerPolicy playerPolicy, 
                         EventCommandsPolicy commandsPolicy, EventLocations locations) {

        super(plugin, name, description, reward, maxTimeInSeconds, itemPolicy, playerPolicy, commandsPolicy,
                locations,
                EventMessages.builder()
                        .started(Arrays.asList("", "&eEvento " + name + " iniciado.", "${dynamic_description}", ""))
                        .status(Arrays.asList("", "${dynamic_status}", ""))
                        .willStartTimes(0)
                        .stopDelay(0)
                        .build());
        this.icon = icon;

        addPlaceHolder("dynamic_description", this::getDynamicDescription);
        addPlaceHolder("dynamic_status", this::getDynamicStatus);
    }

    public boolean tryEvent(Player player, T parameter) {
        val result = runTry(player, parameter);
        if (result)
            end(new EventWinner<>(player, player.getName()));

        return result;
    }

    public abstract String getDynamicDescription();

    public abstract String getDynamicStatus();

    protected abstract boolean runTry(Player player, T parameter);

}
