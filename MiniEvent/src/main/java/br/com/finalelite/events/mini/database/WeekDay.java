package br.com.finalelite.events.mini.database;

public enum WeekDay {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAYS,
    THURSDAY,
    FRIDAY,
    SATURDAY;


    public WeekDay fromOrdinal(int ordinal) {
        if (ordinal > WeekDay.values().length)
            return null;
        return WeekDay.values()[ordinal];
    }
}
