package br.com.finalelite.events.mini.database;

import com.gitlab.pauloo27.core.sql.*;
import lombok.val;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private final EzSQL sql;
    private final EzTable historyTable;

    public Database(EzSQL sql) {
        this.sql = sql;
        this.historyTable = getHistoryTable();
    }

    private EzTable getHistoryTable() {
        try {
            return sql.createIfNotExists(new EzTableBuilder("events")
                    .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                    .withColumn(new EzColumnBuilder("weekDay", EzDataType.TINYINT, EzAttribute.NOT_NULL))
                    .withColumn(new EzColumnBuilder("dayPeriod", EzDataType.TINYINT, EzAttribute.NOT_NULL))
                    .withColumn(new EzColumnBuilder("eventType", EzDataType.VARCHAR, 36, EzAttribute.NOT_NULL))
                    .withColumn(new EzColumnBuilder("playersOnlineAtStart", EzDataType.INTEGER, EzAttribute.NOT_NULL)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Integer> getPlayerAverageByWeekDay(WeekDay day, DayPeriod period) {
        try (val result = historyTable.select(new EzSelect("playerOnlineAtStart")
                .where().equals("weekDay", day.ordinal())
                .and().equals("dayPeriod", period.ordinal())
                .limit(5)).getResultSet()) {

            val players = new ArrayList<Integer>();

            while (result.next()) {
                players.add(result.getInt("playerOnlineAtStart"));
            }

            return players;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void insertEventInTheHistory() {
        // TODO
    }

}
