package br.com.finalelite.events.mini;

import br.com.finalelite.events.base.EventManager;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MiniEventManager {
    private final CommandablePlugin plugin;
    private final EventManager eventManager;
    private List<BaseMiniEvent> events = new ArrayList<>();
    private List<BaseMiniEvent> alreadyOccurred = new ArrayList<>();
    private Map<BaseMiniEvent, Integer> votes;
    private List<Player> votedPlayers;
    private BaseMiniEvent runningEvent;

    public void registerEvent(BaseMiniEvent event) {
        events.add(event);
        eventManager.register(event);
    }

    public byte startVote() {
        if (isOpenToVote())
            return -1;
        if (hasEventRunning())
            return -2;

        votes = new HashMap<>();
        votedPlayers = new ArrayList<>();

        return 0;
    }

    public boolean vote(Player player, BaseMiniEvent event) {
        if (votedPlayers.contains(player))
            return false;
        if (votes.containsKey(event))
            votes.put(event, votes.get(event) + 1);
        else
            votes.put(event, 1);
        votedPlayers.add(player);
        return true;
    }

    public boolean hasVoted(Player player) {
        return votedPlayers.contains(player);
    }

    public boolean isOpenToVote() {
        return votes != null;
    }

    public void openVoteInventory(Player player) {
        if (!isOpenToVote())
            return;

        val gui = new BaseGUI() {
            @Override
            public Inventory open() {
                val inventory = Bukkit.createInventory(null, 5 * 9, MiniEventManager.this.hasVoted(player) ? "Você já votou" : "Clique em um evento para votar");

                val events = new ArrayList<>(MiniEventManager.this.events);
                events.removeAll(MiniEventManager.this.alreadyOccurred);

                events.forEach(event ->
                        addItem(inventory, event.getIcon(), MiniEventManager.this.getVotes(event) == 0 ? 1 : MiniEventManager.this.getVotes(event), ChatColor.AQUA + event.getName(), Arrays.asList("", "&7" + event.getDescription(), "", "&bClique para votar."),
                                (clickEvent) -> {
                                    val player = (Player) clickEvent.getWhoClicked();
                                    if (!MiniEventManager.this.isOpenToVote()) {
                                        player.sendMessage(ChatColor.RED + "Votação já encerrada.");
                                        player.closeInventory();
                                        return;
                                    }
                                    val result = MiniEventManager.this.vote(player, event);
                                    if (result)
                                        player.sendMessage(String.format("%sVocê votou para %s.", ChatColor.RED, event.getName()));
                                    else
                                        player.sendMessage(String.format("%sVocê não pode votar de novo.", ChatColor.RED));

                                    player.closeInventory();
                                }));


                return inventory;
            }
        };

        PauloAPI.getInstance().getGuiManager().open(player, gui);
    }

    private int getVotes(BaseMiniEvent event) {
        return votes.getOrDefault(event, 0);
    }

    public AbstractMap.SimpleEntry<Byte, BaseMiniEvent> endVote() {
        val sortedMap = sortMap(votes, 10);
        if (sortedMap.values().size() == 0)
            return null;

        val maxVotes = sortedMap.values().iterator().next();
        if (maxVotes == 0)
            return null;

        val maxVoted = sortedMap.entrySet().stream().filter(entry -> entry.getValue().equals(maxVotes)).collect(Collectors.toList());

        votedPlayers = null;
        votes = null;

        if (maxVoted.size() == 1)
            return new AbstractMap.SimpleEntry<>((byte) 1, maxVoted.get(0).getKey());
        else
            return new AbstractMap.SimpleEntry<>((byte) maxVoted.size(), maxVoted.get(0).getKey());
    }

    public BaseMiniEvent getRunningEvent() {
        if (eventManager.getInGameOrWaitingEvents().size() == 1) {
            val event = eventManager.getInGameOrWaitingEvents().get(0);
            if (event instanceof BaseMiniEvent)
                return (BaseMiniEvent) event;
        }

        if (runningEvent == null || runningEvent.isIdle())
            return null;
        return runningEvent;
    }

    public boolean hasEventRunning() {
        return getRunningEvent() != null;
    }

    public String startEvent(BaseMiniEvent event) {
        runningEvent = event;
        alreadyOccurred.add(event);
        return event.start();
    }

    public List<BaseMiniEvent> getEvents() {
        return new ArrayList<>(events);
    }

    public <T> Map<T, Integer> sortMap(Map<T, Integer> map, int limit) {
        val sortedList = map.entrySet()
                .stream()
                .distinct()
                .limit(limit)
                .sorted(Comparator.comparingInt(o -> (int) o.getValue())).collect(Collectors.toList());
        // reverse
        Collections.reverse(sortedList);

        // create a map
        val sorted = new LinkedHashMap<T, Integer>();
        sortedList.forEach(entry -> sorted.put(entry.getKey(), entry.getValue()));
        return sorted;
    }

    public BaseMiniEvent getEvent(String name) {
        return events.stream().filter(baseMiniEvent -> baseMiniEvent.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}
