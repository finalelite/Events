package br.com.finalelite.events.mini.database;

public enum DayPeriod {
    MORNING,
    EVENING,
    NIGHT;

    public DayPeriod fromOrdinal(int ordinal) {
        if (ordinal > DayPeriod.values().length)
            return null;
        return DayPeriod.values()[ordinal];
    }
}
