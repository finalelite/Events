package br.com.finalelite.events.base;

public enum EventStatus {
    IDLE,
    WAITING,
    IN_GAME,
    ENDING,
}
