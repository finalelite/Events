package br.com.finalelite.events.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EventManager {
    private Map<String, BaseEvent> events = new HashMap<>();

    public boolean isValid(String name) {
        return !events.containsKey(name) && !name.equalsIgnoreCase("list");
    }

    public boolean register(BaseEvent event) {
        if (!isValid(event.getName()))
            return false;
        events.put(event.getName().toLowerCase(), event);
        return true;
    }

    public BaseEvent getEvent(String name) {
        return events.getOrDefault(name.toLowerCase(), null);
    }

    public List<BaseEvent> getEvents() {
        return new ArrayList<>(events.values());
    }

    public List<BaseEvent> getInGameEvents() {
        return events.values().stream().filter(event -> event.getStatus() == EventStatus.IN_GAME).collect(Collectors.toList());
    }

    public List<BaseEvent> getWaitingEvents() {
        return events.values().stream().filter(event -> event.getStatus() == EventStatus.WAITING).collect(Collectors.toList());
    }

    public List<BaseEvent> getInGameOrWaitingEvents() {
        return events.values().stream().filter(event -> event.getStatus() == EventStatus.WAITING || event.getStatus() == EventStatus.IN_GAME).collect(Collectors.toList());
    }
}
