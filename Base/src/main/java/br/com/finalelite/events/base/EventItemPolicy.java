package br.com.finalelite.events.base;

public enum EventItemPolicy {
    PLAYER_ITEMS,
    GIVE_ITEMS,
}
