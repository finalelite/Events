package br.com.finalelite.events.base;

import lombok.Data;

@Data
public class EventWinner<T> {
    private final T winner;
    private final String name;
}
