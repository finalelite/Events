package br.com.finalelite.events.base;

import lombok.Builder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Builder
public class EventCommandsPolicy {

    private List<String> commands;
    private boolean whitelist;

    public static EventCommandsPolicy onlyEventCommand() {
        return new EventCommandsPolicy(Arrays.asList("evento", "eventos"), true);
    }

    public static EventCommandsPolicy allCommands() {
        return new EventCommandsPolicy(Collections.singletonList("*"), true);
    }

    public static EventCommandsPolicyBuilder builder() {
        return new EventCommandsPolicyBuilder();
    }

    public boolean isAllowed(String command) {
        return (commands.contains(command) || commands.contains("*")) && whitelist;
    }

}
