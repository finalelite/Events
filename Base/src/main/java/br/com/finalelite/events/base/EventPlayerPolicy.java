package br.com.finalelite.events.base;

import lombok.Data;

@Data
public class EventPlayerPolicy {
    private final int minPlayers;
    private final int maxPlayers;

    public static EventPlayerPolicy getDefaultPlayerPolicy() {
        return new EventPlayerPolicy(2, -1);
    }

    public boolean hasMax() {
        return maxPlayers != -1;
    }

    public boolean hasMin() {
        return minPlayers != -1;
    }

    public static EventPlayerPolicy noLimit() {
        return new EventPlayerPolicy(-1, -1);
    }

}
