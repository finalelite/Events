package br.com.finalelite.events.base.listeners;

import br.com.finalelite.events.base.BaseEvent;
import br.com.finalelite.events.base.EventFlag;
import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

@RequiredArgsConstructor
public class PlayerListener implements Listener {

    private final BaseEvent event;

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        val player = event.getPlayer();

        if (this.event.getPlayerStatus(player) == EventPlayerStatus.ALIVE) {
            this.event.lose(player, "Disconectado");
        } else if (this.event.getPlayerStatus(player) == EventPlayerStatus.WAITING) {
            this.event.leave(player);
        } else if (this.event.getPlayerStatus(player).isSpectating()) {
            this.event.leaveSpectating(player);
        }
        player.setGameMode(GameMode.SURVIVAL);
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        if (event.getCause() != PlayerTeleportEvent.TeleportCause.SPECTATE)
            return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity().getType() != EntityType.PLAYER)
            return;

        val player = (Player) event.getEntity();

        if (this.event.getPlayerStatus(player) == EventPlayerStatus.WAITING || this.event.getPlayerStatus(player).isSpectating()
                || (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE && this.event.hasFlag(EventFlag.DISABLE_DAMAGE))) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamageByPlayer(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() != EntityType.PLAYER || event.getDamager().getType() != EntityType.PLAYER)
            return;

        val player = (Player) event.getEntity();

        if (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE && this.event.hasFlag(EventFlag.DISABLE_PVP))
            event.setCancelled(true);
    }

    @EventHandler
    public void onFoodLoss(FoodLevelChangeEvent event) {
        val player = (Player) event.getEntity();
        if (!this.event.hasFlag(EventFlag.DISABLE_HUNGER) || this.event.getPlayerStatus(player) == EventPlayerStatus.IDLE)
            return;

        val newLevel = event.getFoodLevel();
        val oldLevel = player.getFoodLevel();

        if (newLevel < oldLevel)
            event.setCancelled(true);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        val player = event.getPlayer();

        if (this.event.getPlayerStatus(player).isSpectating() || this.event.getPlayerStatus(player) == EventPlayerStatus.WAITING) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    player.teleport(PlayerListener.this.event.getLocations().getSpectating());
                }
            }.runTaskLaterAsynchronously(this.event.getPlugin(), 20);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        val player = event.getPlayer();

        if (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE)
            event.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        val player = event.getPlayer();

        if (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        String command = event.getMessage().split(" ")[0].substring(1);
        if (this.event.getPlayerStatus(event.getPlayer()) == EventPlayerStatus.IDLE)
            return;

        if (this.event.getCommandsPolicy().isAllowed(command.toLowerCase()))
            return;

        MessageUtils.sendColouredMessage(event.getPlayer(), "&cVocê não pode usar esse comando no evento.");
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMoveEvent(PlayerMoveEvent event) {
        if (this.event.isIdle())
            return;

        val player = event.getPlayer();

        if (!this.event.getPlayerStatus(player).isSpectating() &&
                (this.event.getPlayerStatus(player) == EventPlayerStatus.IDLE || !this.event.hasFlag(EventFlag.DISABLE_ARENA_LEAVE)))
            return;

        if (!this.event.isInArena(player)) {
            if (this.event.getPlayerStatus(player).isSpectating())
                player.teleport(this.event.getLocations().getSpectating());
            else
                player.teleport(this.event.getLocations().getGame());
            player.sendMessage(ChatColor.BLUE + "Você não pode sair da arena.");
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (!this.event.hasFlag(EventFlag.DISABLE_ITEM_DROP))
            return;

        val player = event.getPlayer();
        if (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE)
            event.setCancelled(true);
    }

    @EventHandler
    public void onPickup(PlayerAttemptPickupItemEvent event) {
        if (!this.event.hasFlag(EventFlag.DISABLE_ITEM_PICKUP))
            return;

        val player = event.getPlayer();
        if (this.event.getPlayerStatus(player) != EventPlayerStatus.IDLE)
            event.setCancelled(true);
    }


}
