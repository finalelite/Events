package br.com.finalelite.events.base;

public enum EventPlayerStatus {
    WAITING,
    ALIVE,
    SPECTATING,
    DIED_SPECTATING,
    IDLE;

    public boolean isSpectating() {
        return this == SPECTATING || this == DIED_SPECTATING;
    }
}

