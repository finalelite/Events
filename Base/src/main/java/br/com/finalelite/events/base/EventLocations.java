package br.com.finalelite.events.base;

import br.com.finalelite.pauloo27.api.config.ConfigUtils;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
@Getter
public class EventLocations {

    private Location spawn;
    private Location waiting;
    private Location spectating;
    private Location game;
    private Location pos1, pos2;
    @Builder.Default
    @Setter
    private boolean teleportToGameOnStart = true;
    private Map<String, Location> customLocations;
    private boolean valid;

    public static EventLocations getFromConfig(FileConfiguration config, String root) {
        return getFromConfig(config, root, null);
    }

    public static EventLocations getFromConfig(FileConfiguration config, String root, List<String> customLocations) {
        try {
            val locations = new HashMap<String, Location>();
            if (customLocations != null)
                customLocations.forEach(s -> locations.put(MessageUtils.capitalize(s),
                        ConfigUtils.deserializeLocation(getFromKey(config, root, MessageUtils.capitalize(s)))));

            return new EventLocations(
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Spawn")),
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Waiting")),
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Spectating")),
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Game")),
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Pos1")),
                    ConfigUtils.deserializeLocation(getFromKey(config, root, "Pos2")),
                    false,
                    customLocations == null ? null : locations, true);
        } catch (NullPointerException e) {
            val map = new HashMap<String, Location>();
            if (customLocations != null) {
                customLocations.forEach(s -> map.put(s, null));
            }
            return new EventLocations(null, null, null, null, null, null, false, customLocations == null ? null : map, false);
        }
    }

    private static String getFromKey(FileConfiguration config, String root, String name) {
        return config.getString(root + "." + name);
    }

    public static EventLocations empty() {
        return new EventLocationsBuilder().valid(true).build();
    }

    public static EventLocationsBuilder builder() {
        return new EventLocationsBuilder();
    }

    public Location getCustomLocation(String name) {
        if (customLocations == null)
            return null;
        return customLocations.getOrDefault(MessageUtils.capitalize(name), null);
    }

}
