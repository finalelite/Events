package br.com.finalelite.events.base;

@FunctionalInterface
public interface MessagePlaceHolder {
    String get();
}
