package br.com.finalelite.events.base;

import lombok.Data;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@Data
public class EventReward<T> {
    private final String name;
    private final Action<T> action;

    public interface Action<T> {
        void run(EventWinner<T> winner);
    }

    public static EventReward moneyReward(int amount) {
        return new EventReward<Player>("$" + amount,
                (winner) -> winner.getWinner().sendMessage(ChatColor.GREEN + "*você recebe $" + amount + "."));
    }

    public static EventReward itemReward(ItemStack item) {
        return new EventReward<Player>(item.getAmount() + "x " + item.getType().name(),
                (winner) -> winner.getWinner().getInventory().addItem(item));
    }
}
