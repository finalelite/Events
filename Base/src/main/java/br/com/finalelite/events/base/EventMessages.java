package br.com.finalelite.events.base;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Builder
@Getter
@Setter
public class EventMessages {
    @Builder.Default
    private List<String> willStart = Arrays.asList("", "&eEvento &f${event_name} &eirá iniciar em &f${seconds_to_start} segundos&e.",
            "&f${players_waiting} já estão no evento.", "");
    @Builder.Default
    private int willStartTimes = 2;
    @Builder.Default
    private int willStartDelay = 5;
    @Builder.Default
    private List<String> started = Arrays.asList("", "&eEvento &f${event_name} &einiciado com &f${players_alive}.", "");
    @Builder.Default
    private List<String> ended = Arrays.asList("", "&eEvento encerrado, o vencedor foi &f${winner}&e.", "");
    @Builder.Default
    private List<String> cancelled = Arrays.asList("", "&eEvento cancelado.", "&eMotivo: &f${cancel_reason}&e.", "");
    @Builder.Default
    private List<String> notEnoughPlayer = Arrays.asList("", "&cJogadores insuficientes. Evento cancelado.", "");
    @Builder.Default
    private List<String> completeStatus = Arrays.asList("", "&eJogadores: &f{players}&e.", "");
    @Builder.Default
    private List<String> willTimeout = Arrays.asList("", "&eO evento será encerrado em {time_to_timeout} se ninguém vencer até lá.", "");
    @Builder.Default
    private List<String> timeout = Arrays.asList("", "&eO tempo máximo do evento foi exedido. O jogador com mais abates será o vencedor.", "");
    @Builder.Default
    private List<String> status = Arrays.asList("", "&eO evento &f${event_name} &ejá está acontecendo a &f${event_time}&e.",
            "&f${players_alive} vivos.", "");
    @Builder.Default
    private int statusDelay = 20;
    @Builder.Default
    private int stopDelay = 20;

    public static EventMessagesBuilder builder() {
        return new EventMessagesBuilder();
    }

    public static EventMessages getDefaultMessages() {
        return builder().build();
    }

}
