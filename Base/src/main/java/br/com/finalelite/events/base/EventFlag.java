package br.com.finalelite.events.base;

public enum EventFlag {
    DISABLE_ITEM_DROP,
    DISABLE_ITEM_PICKUP,
    DISABLE_PVP,
    DISABLE_DAMAGE,
    DISABLE_HUNGER,
    DISABLE_ARENA_LEAVE
}
