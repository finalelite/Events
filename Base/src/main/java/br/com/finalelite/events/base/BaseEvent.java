package br.com.finalelite.events.base;

import br.com.finalelite.events.base.listeners.PlayerListener;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import br.com.finalelite.pauloo27.api.utils.MapUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class BaseEvent {
    @Getter
    private final String name;
    @Getter
    private final String description;
    @Getter
    public long maxTimeInSeconds;
    @Getter
    private EventStatus status = EventStatus.IDLE;
    @Getter
    private EventItemPolicy itemPolicy;
    @Getter
    private EventPlayerPolicy playerPolicy;
    @Getter
    private EventMessages messages;
    @Getter
    @Setter
    private EventLocations locations;
    @Getter
    private EventCommandsPolicy commandsPolicy;
    @Getter
    private Date startTime;
    @Getter
    private Date realStarTime;
    @Getter
    @Setter
    private List<ItemStack> items;
    @Getter
    private String cancelReason;
    @Getter
    private EventReward reward;
    @Getter
    private EventWinner winner;
    @Getter
    private List<EventFlag> flags = new ArrayList<>();
    @Getter
    private int playersCountOnStart;
    @Getter
    private JavaPlugin plugin;
    private Map<Player, Integer> score = new HashMap<>();
    private BukkitTask willStartTask;
    private Map<Player, EventPlayerStatus> playersStatus = new HashMap<>();

    private Map<String, MessagePlaceHolder> placeHolders = new HashMap<>();

    public BaseEvent(JavaPlugin plugin, String name, String description, EventReward reward, long maxTimeInSeconds, EventItemPolicy itemPolicy, EventPlayerPolicy playerPolicy, EventCommandsPolicy commandsPolicy, EventLocations locations, EventMessages messages) {
        this.plugin = plugin;
        this.name = name;
        this.description = description;
        this.reward = reward;
        this.maxTimeInSeconds = maxTimeInSeconds;
        this.itemPolicy = itemPolicy;
        this.playerPolicy = playerPolicy;
        this.commandsPolicy = commandsPolicy;
        this.locations = locations;
        this.messages = messages;
        registerPlaceHolders();

        plugin.getServer().getPluginManager().registerEvents(new PlayerListener(this), plugin);
    }

    public void addPlaceHolder(String key, MessagePlaceHolder placeHolder) {
        placeHolders.put(key.toLowerCase(), placeHolder);
    }

    public void addFlag(EventFlag... flag) {
        Collections.addAll(this.flags, flag);
    }

    public boolean hasFlag(EventFlag flag) {
        return this.flags.contains(flag);
    }

    public EventPlayerStatus getPlayerStatus(Player player) {
        return playersStatus.getOrDefault(player, EventPlayerStatus.IDLE);
    }

    public List<Player> getAlivePlayers() {
        return playersStatus.entrySet().stream().filter(entry -> entry.getValue() == EventPlayerStatus.ALIVE)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Player> getWaitingPlayers() {
        return playersStatus.entrySet().stream().filter(entry -> entry.getValue() == EventPlayerStatus.WAITING)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public boolean isInGame() {
        return status == EventStatus.IN_GAME;
    }

    public boolean isWaiting() {
        return status == EventStatus.WAITING;
    }

    public boolean isIdle() {
        return status == EventStatus.IDLE;
    }

    public String join(Player player) {
        if (getPlayerStatus(player) != EventPlayerStatus.IDLE || !isWaiting())
            return "Você já está no evento.";
        val msg = onJoin(player);
        if (msg != null) {
            return msg;
        }
        playersStatus.put(player, EventPlayerStatus.WAITING);
        teleport(player, locations.getWaiting());
        if (itemPolicy == EventItemPolicy.GIVE_ITEMS) {
            player.getInventory().clear();
            if (items != null)
                player.getInventory().addItem(items.toArray(new ItemStack[1]));
        }
        player.setFoodLevel(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        player.setGameMode(GameMode.ADVENTURE);
        MessageUtils.sendColouredMessage(player, String.format("&aVocê entrou no evento &f%s&a.", name));
        return null;
    }

    public String joinSpectating(Player player) {
        if (isIdle() || getPlayerStatus(player) != EventPlayerStatus.IDLE)
            return "O evento não está disponivel ou você já está nele.";
        val msg = onJoinSpectating();
        if (msg != null) {
            return msg;
        }
        teleport(player, locations.getSpectating());
        playersStatus.put(player, EventPlayerStatus.SPECTATING);
        if (locations.getPos1() != null && locations.getPos2() != null) {
            player.setGameMode(GameMode.SPECTATOR);
        }
        MessageUtils.sendColouredMessage(player, String.format("&aVocê entrou como espectador no evento &f%s&a.", name));
        return null;
    }

    public String leaveSpectating(Player player) {
        if (isIdle() || !getPlayerStatus(player).isSpectating())
            return "Você não está espectando o evento.";
        val msg = onLeaveSpectating();
        if (msg != null) {
            return msg;
        }
        teleport(player, locations.getSpawn());
        playersStatus.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        MessageUtils.sendColouredMessage(player, String.format("&aVocê não é mais espectador do evento &f%s&a.", name));
        return null;
    }

    public String leave(Player player) {
        if (getPlayerStatus(player) == EventPlayerStatus.IDLE || getStatus() != EventStatus.WAITING)
            return "Você não está no evento.";
        val msg = onLeave(player);
        if (msg != null) {
            MessageUtils.sendColouredMessage(player, msg);
            return msg;
        }
        playersStatus.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        MessageUtils.sendColouredMessage(player, String.format("&cVocê saiu do evento &f%s&a.", name));
        teleport(player, locations.getSpawn());
        return null;
    }

    public void score(Player player, int score) {
        if (getPlayerStatus(player) != EventPlayerStatus.ALIVE)
            return;
        this.score.put(player, this.score.getOrDefault(player, 0) + score);
        onScore(player, score);
    }

    public String start() {
        if (!isIdle())
            return "Evento já acontecendo.";
        if (getLocations() == null || !getLocations().isValid())
            return "Localização não definida. Use /evento warp <evento> <warp>.";
        val msg = onStarting();
        if (msg != null) {
            return msg;
        }
        status = EventStatus.WAITING;
        startTime = new Date();
        AtomicInteger times = new AtomicInteger(0);
        val limitTimes = messages.getWillStartTimes();
        val delay = messages.getWillStartDelay();
        if (limitTimes == 0)
            forceStart();
        else {
            willStartTask = new BukkitRunnable() {
                @Override
                public void run() {
                    if (times.incrementAndGet() == limitTimes + 1) {
                        forceStart();
                        return;
                    }
                    announce(messages.getWillStart());
                }
            }.runTaskTimer(plugin, 0, delay * 20);
        }
        return null;
    }

    public String onStarting() {
        return null;
    }

    public String onStart() {
        return null;
    }

    public void onScore(Player player, int score) {
    }

    public void onStop() {
    }

    public void onEnd(EventWinner winner) {
    }

    public void onCancel(String reason) {
    }

    public String onJoin(Player player) {
        return null;
    }

    public String onLeave(Player player) {
        return null;
    }

    public String onJoinSpectating() {
        return null;
    }

    public String onLeaveSpectating() {
        return null;
    }

    public void onLose(Player player, String reason) {
    }

    public void announce(List<String> messages) {
        messages.forEach(message -> Bukkit.broadcastMessage(translatePlaceHolders(message)));
    }

    public List<String> prepareMessage(List<String> messages) {
        return messages.stream().map(this::translatePlaceHolders).collect(Collectors.toList());
    }

    public List<String> getEventStatus() {
        return messages.getCompleteStatus();
    }

    public boolean lose(Player player, String reason) {
        if (getPlayerStatus(player) != EventPlayerStatus.ALIVE)
            return false;
        MessageUtils.sendColouredMessage(player, "&cVocê perdeu! " + reason);
        playersStatus.put(player, EventPlayerStatus.DIED_SPECTATING);
        teleport(player, locations.getSpectating());
        onLose(player, reason);
        return true;
    }

    public boolean forceStart() {
        if (status != EventStatus.WAITING && status != EventStatus.IDLE)
            return false;
        if (willStartTask != null && !willStartTask.isCancelled())
            willStartTask.cancel();
        if (playerPolicy.hasMin() && playerPolicy.getMinPlayers() > getWaitingPlayers().size()) {
            stop();
            announce(messages.getNotEnoughPlayer());
            return false;
        }
        val msg = onStart();
        if (msg != null) {
            stop();
            announce(Arrays.asList(msg.split("\n")));
            return false;
        }
        status = EventStatus.IN_GAME;
        realStarTime = new Date();
        getWaitingPlayers().forEach(p -> {
            playersStatus.put(p, EventPlayerStatus.ALIVE);
            if (locations.isTeleportToGameOnStart())
                teleport(p, locations.getGame());
            p.setGameMode(GameMode.ADVENTURE);
        });
        announce(messages.getStarted());
        // TODO Create just one  thread for all the tasks
        if (messages.getStatusDelay() > 0) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!isInGame()) {
                        cancel();
                        return;
                    }
                    announce(messages.getStatus());
                }
            }.runTaskTimer(plugin, messages.getStatusDelay() * 20, messages.getStatusDelay() * 20);
        }
        if (maxTimeInSeconds > 0) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (getTotalTime() >= maxTimeInSeconds) {
                        end(onTimeMax());
                        cancel();
                    }
                }
            }.runTaskTimer(plugin, 20 * 20, 20 * 20);
        }
        playersCountOnStart = getAlivePlayers().size();
        return true;
    }

    protected EventWinner onTimeMax() {
        if (score == null || score.size() == 0)
            return null;
        val topPlayer = getTopPlayers(1).entrySet().stream().findFirst().get().getKey();
        return new EventWinner<>(topPlayer, topPlayer.getName());
    }

    public boolean end(EventWinner winner) {
        this.winner = winner;
        announce(messages.getEnded());
        onEnd(winner);
        reward.getAction().run(winner);
        val temp = new HashMap<Player, EventPlayerStatus>();
        playersStatus.keySet().forEach(p -> temp.put(p, EventPlayerStatus.WAITING));
        playersStatus = temp;
        status = EventStatus.ENDING;
        if (messages.getStopDelay() < 1) {
            stop();
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    stop();
                }
            }.runTaskLater(plugin, 20 * messages.getStopDelay());
        }
        return true;
    }

    public boolean cancel(String reason) {
        if (isIdle())
            return false;
        cancelReason = reason;
        announce(messages.getCancelled());
        onCancel(reason);
        return stop();
    }

    private boolean stop() {
        if (willStartTask != null && !willStartTask.isCancelled())
            willStartTask.cancel();
        onStop();
        status = EventStatus.IDLE;
        playersStatus.keySet().forEach(p -> {
            teleport(p, locations.getSpawn());
            p.setGameMode(GameMode.SURVIVAL);
        });
        playersStatus.clear();
        score.clear();
        startTime = null;
        realStarTime = null;
        return true;
    }

    private int getReamingSecondsToStart() {
        if (startTime != null && realStarTime == null)
            return (int) ((messages.getWillStartTimes() * messages.getWillStartDelay()) - ((new Date().getTime() - startTime.getTime()) / 1000));
        else
            return 0;
    }

    private int getTotalTime() {
        if (realStarTime != null)
            return (int) ((new Date().getTime() - realStarTime.getTime()) / 1000);
        else
            return 0;
    }

    public void teleport(Player player, Location location) {
        if (location == null)
            return;
        player.teleport(location);
    }

    public String getTotalTimeFormatted() {
        val time = getTotalTime();
        val s = (time % 60 == 0 ? "" : " e " + time % 60 + " segundos");
        if (time >= 120)
            return time / 60 + " minutos" + s;
        if (time >= 60)
            return time / 60 + " minuto" + s;
        else
            return time + " segundos";
    }


    public EventWinner getWinner() {
        return winner;
    }

    public String getWinnerName() {
        return winner == null ? "Ninguém" : winner.getName();
    }

    public String getPlayersForCompleteStatus() {
        return playersStatus.keySet().stream().map(Player::getName).collect(Collectors.joining());
    }

    public int getPlayerScore(Player player) {
        return score.getOrDefault(player, 0);
    }

    public Map<Player, Integer> getTopPlayers(int limit) {
        return MapUtils.sortMap(score, limit);
    }

    public Map<Player, Integer> getPlayersScore() {
        return new HashMap<>(score);
    }

    public void registerPlaceHolders() {
        addPlaceHolder("event_name", this::getName);
        addPlaceHolder("seconds_to_start", () -> String.valueOf(getReamingSecondsToStart()));
        addPlaceHolder("players_alive", () -> MessageUtils.pluralize(getAlivePlayers().size(), "&ejogador", "&ejogadores"));
        addPlaceHolder("players_waiting", () -> MessageUtils.pluralize(getWaitingPlayers().size(), "&ejogador", "&ejogadores"));
        addPlaceHolder("event_time", this::getTotalTimeFormatted);
        addPlaceHolder("players", this::getPlayersForCompleteStatus);
        addPlaceHolder("cancel_reason", () -> getCancelReason() == null ? "" : getCancelReason());
        addPlaceHolder("winner", this::getWinnerName);
    }

    private String translatePlaceHolders(String message) {
        val pattern = Pattern.compile("\\$\\{\\w*}");
        val matcher = pattern.matcher(message);
        var newText = message;

        while (matcher.find()) {
            val group = matcher.group();
            val key = group.substring(2, group.length() - 1);

            if (placeHolders.containsKey(key.toLowerCase())) {
                val newString = placeHolders.get(key.toLowerCase()).get();
                newText = newText.replace(group, newString);
            }
        }
        return ChatColor.translateAlternateColorCodes('&', newText);
    }

    private boolean checkPosition(double pos1, double pos2, double playerPos) {
        val max = Math.max(pos1, pos2);
        val min = Math.min(pos1, pos2);

        return playerPos < max && playerPos > min;
    }

    public boolean isInArena(Player player) {
        val pos1 = locations.getPos1();
        val pos2 = locations.getPos2();
        val playerPos = player.getLocation();

        val x = checkPosition(pos1.getX(), pos2.getX(), playerPos.getX());
        val y = checkPosition(pos1.getY(), pos2.getY(), playerPos.getY());
        val z = checkPosition(pos1.getZ(), pos2.getZ(), playerPos.getZ());

        return x && y && z && pos1.getWorld() == playerPos.getWorld();
    }
}
