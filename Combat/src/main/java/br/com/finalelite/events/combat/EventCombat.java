package br.com.finalelite.events.combat;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.combat.listeners.PlayerListener;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class EventCombat extends BaseEvent {

    public EventCombat(JavaPlugin plugin) {
        super(plugin, "Combate", "Mata-mata",
                new EventReward("1 diamante",
                        winner -> ((Player) winner.getWinner()).getInventory().addItem(new ItemStack(Material.DIAMOND))),
                60 * 30, EventItemPolicy.GIVE_ITEMS,
                new EventPlayerPolicy(2, -1), EventCommandsPolicy.onlyEventCommand(),
                EventLocations.getFromConfig(plugin.getConfig(), "Events.Gladiador.Locations"), EventMessages.builder().build());

        setItems(Arrays.asList(new ItemStack(Material.DIAMOND_SWORD),
                new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 2),
                new ItemStack(Material.DIAMOND_HELMET),
                new ItemStack(Material.DIAMOND_CHESTPLATE),
                new ItemStack(Material.DIAMOND_LEGGINGS),
                new ItemStack(Material.DIAMOND_BOOTS)));

        plugin.getServer().getPluginManager().registerEvents(new PlayerListener(this), plugin);
    }

    @Override
    public void onLose(Player player, String reason) {
        if (getAlivePlayers().size() == 1) {
            end(new EventWinner<>(getAlivePlayers().get(0), getAlivePlayers().get(0).getName()));
        }
    }
}
