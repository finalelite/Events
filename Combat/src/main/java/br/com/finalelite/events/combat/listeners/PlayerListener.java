package br.com.finalelite.events.combat.listeners;

import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.events.combat.EventCombat;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

@AllArgsConstructor
public class PlayerListener implements Listener {
    private final EventCombat combat;

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        val player = event.getEntity();
        if (combat.isIdle() || combat.isWaiting()) {
            return;
        }

        if (combat.getPlayerStatus(player) == EventPlayerStatus.ALIVE) {
            combat.lose(player,
                    event.getEntity().getKiller() == null ? "Morto." : "Morto por " + event.getEntity().getKiller());
        }
    }

}
