package br.com.finalelite.events.lottery;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.lottery.commands.LotteryCommand;
import br.com.finalelite.events.mini.BaseMiniEvent;
import br.com.finalelite.events.mini.MiniEventManager;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Random;

public class LotteryEvent extends BaseMiniEvent<Integer> {

    @Getter
    private int maxNumber, correctNumber, triesCount;

    public LotteryEvent(CommandablePlugin plugin) {
        super(plugin, "Loteria", "Acerte o número sorteado.", Material.EMERALD,
                EventReward.moneyReward(1000),
                5 * 60, EventItemPolicy.PLAYER_ITEMS,
                new EventPlayerPolicy(-1, -1), EventCommandsPolicy.allCommands(), EventLocations.empty());

        plugin.registerCommand(new LotteryCommand(this));
        this.getMessages().setEnded(Arrays.asList("", "&eEvento encerrado, o vencedor foi &f${winner}&e.", "&eO número correto era &f${correct_number}&e.", ""));
        addPlaceHolder("correct_number", () -> String.valueOf(getCorrectNumber()));
    }

    @Override
    public String onStart() {
        maxNumber = Math.max(50, Bukkit.getOnlinePlayers().size() * 8);
        correctNumber = new Random().nextInt(maxNumber);
        return null;
    }

    @Override
    public String getDynamicDescription() {
        return "&eAcerte o número sorteado entre 0 e " + maxNumber + " (talvez " + correctNumber + ").";
    }

    @Override
    public String getDynamicStatus() {
        return getDynamicDescription() + "\n&eTentativas: &f" + triesCount + "&e.";
    }

    @Override
    protected boolean runTry(Player player, Integer parameter) {
        triesCount++;
        return parameter == correctNumber;
    }
}
