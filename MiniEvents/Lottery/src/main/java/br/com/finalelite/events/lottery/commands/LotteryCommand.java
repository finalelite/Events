package br.com.finalelite.events.lottery.commands;

import br.com.finalelite.events.lottery.LotteryEvent;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;

public class LotteryCommand extends BaseCommand {

    public LotteryCommand(LotteryEvent event) {
        super("loteria");
        usage.addParameter(new Parameter("número", DefaultParameterType.INTEGER, true));
        playerListener = cmd -> {
            val player = cmd.getSender();
            if (!event.isInGame()) {
                cmd.reply("&cNenhum evento está acontecendo.");
                return true;
            }

            val number = cmd.getArgumentAsInteger(0);

            val result = event.tryEvent(player, number);
            if (result) {
                cmd.reply("&cVocê acertou (" + number + ")! Parabéns!");
            } else {
                cmd.reply("&cErrado! Continue tentando!");
            }
            return true;
        };
    }
}
