package br.com.finalelite.events.parkour.listeners;

import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.events.parkour.ParkourEvent;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

@RequiredArgsConstructor
public class PlayerListeners implements Listener {

    private final ParkourEvent event;

    @EventHandler
    public void onMove(PlayerMoveEvent event) {

        if (event.getTo().getBlock().getType() != Material.LIGHT_WEIGHTED_PRESSURE_PLATE)
            return;

        if (!this.event.isInGame())
            return;

        if (this.event.getPlayerStatus(event.getPlayer()) != EventPlayerStatus.ALIVE)
            return;

        val result = this.event.tryEvent(event.getPlayer(), event.getTo());
        if (result) {
            event.getPlayer().sendMessage(ChatColor.AQUA + "Parabéns!");
        }
    }

}
