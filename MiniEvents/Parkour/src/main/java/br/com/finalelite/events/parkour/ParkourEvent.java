package br.com.finalelite.events.parkour;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.mini.BaseMiniEvent;
import br.com.finalelite.events.parkour.listeners.PlayerListeners;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import lombok.val;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collections;

public class ParkourEvent extends BaseMiniEvent<Location> {

    public ParkourEvent(CommandablePlugin plugin) {
        super(plugin, "Parkour", "Seja o primeiro a completar o parkour", Material.FIREWORK_ROCKET,
                EventReward.moneyReward(1000), 10 * 60, EventItemPolicy.GIVE_ITEMS,
                new EventPlayerPolicy(2, -1), EventCommandsPolicy.onlyEventCommand(),
                EventLocations.getFromConfig(plugin.getConfig(), "Events.Parkour.Locations", Collections.singletonList("Final")));

        plugin.getServer().getPluginManager().registerEvents(new PlayerListeners(this), plugin);
        getMessages().setWillStartTimes(3);
        getMessages().setWillStartDelay(10);
        getMessages().setStopDelay(-1);

        addFlag(EventFlag.DISABLE_DAMAGE, EventFlag.DISABLE_ITEM_DROP, EventFlag.DISABLE_ARENA_LEAVE, EventFlag.DISABLE_ITEM_PICKUP, EventFlag.DISABLE_HUNGER);
    }

    @Override
    public String getDynamicDescription() {
        return "&eSeja o primeiro a completar o parkour.";
    }

    @Override
    public String getDynamicStatus() {
        return getDynamicDescription();
    }

    @Override
    protected boolean runTry(Player player, Location parameter) {
        val finalLocation = getLocations().getCustomLocation("Final");
        return parameter.getBlock().equals(finalLocation.getBlock());
    }
}
