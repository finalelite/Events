package br.com.finalelite.events.mining.listeners;

import br.com.finalelite.events.mining.MiningEvent;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

@RequiredArgsConstructor
public class PlayerListeners implements Listener {

    private final MiningEvent event;

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled())
            return;

        if (!this.event.isInGame())
            return;

        val player = event.getPlayer();

        this.event.tryEvent(player, event.getBlock().getType());
    }

}
