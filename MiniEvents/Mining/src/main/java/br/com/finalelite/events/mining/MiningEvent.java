package br.com.finalelite.events.mining;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.mini.BaseMiniEvent;
import br.com.finalelite.events.mining.listeners.PlayerListeners;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MiningEvent extends BaseMiniEvent<Material> {

    private Material block;
    private static List<Material> validTypes = Arrays.asList(Material.DIAMOND_ORE, Material.GOLD_ORE, Material.EMERALD_ORE, Material.ANDESITE, Material.STONE, Material.GRAVEL, Material.DIORITE);

    public MiningEvent(JavaPlugin plugin) {
        super(plugin, "Mineração", "Seja o primeiro a quebrar um bloco.", Material.DIAMOND_PICKAXE,
                EventReward.moneyReward(10), 5 * 50, EventItemPolicy.PLAYER_ITEMS, EventPlayerPolicy.noLimit(),
                EventCommandsPolicy.allCommands(), EventLocations.empty());
        plugin.getServer().getPluginManager().registerEvents(new PlayerListeners(this), plugin);
    }

    @Override
    public String onStart() {
        val index = new Random().nextInt(validTypes.size());
        block = validTypes.get(index);
        return null;
    }

    @Override
    public String getDynamicDescription() {
        return "&eSeja o primeiro a quebrar um " + block.name() + ".";
    }

    @Override
    public String getDynamicStatus() {
        return getDynamicDescription();
    }

    @Override
    protected boolean runTry(Player player, Material parameter) {
        return parameter == block;
    }
}
