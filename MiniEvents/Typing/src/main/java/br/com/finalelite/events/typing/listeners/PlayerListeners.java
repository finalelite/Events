package br.com.finalelite.events.typing.listeners;

import br.com.finalelite.events.typing.TypingEvent;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

@RequiredArgsConstructor
public class PlayerListeners implements Listener {

    private final TypingEvent event;

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        val player = event.getPlayer();
        
        if(!this.event.isInGame())
            return;

        val result = this.event.tryEvent(event.getPlayer(), event.getMessage());

        if (result) {
            player.sendMessage(ChatColor.BLUE + "Parabéns!");
            event.setCancelled(true);
        }
    }
}
