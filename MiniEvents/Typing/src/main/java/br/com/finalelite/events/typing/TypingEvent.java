package br.com.finalelite.events.typing;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.mini.BaseMiniEvent;
import br.com.finalelite.events.mini.MiniEventManager;
import br.com.finalelite.events.typing.listeners.PlayerListeners;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class TypingEvent extends BaseMiniEvent<String> {

    private String text;

    public TypingEvent(CommandablePlugin plugin) {
        super(plugin, "Digitação", "Seja o mais rápido a digitar o comando", Material.WRITABLE_BOOK,
              EventReward.moneyReward(1000), 5 * 60, EventItemPolicy.PLAYER_ITEMS, new EventPlayerPolicy(-1, -1), EventCommandsPolicy.allCommands(), EventLocations.empty());

        plugin.getServer().getPluginManager().registerEvents(new PlayerListeners(this), plugin);
    }

    @Override
    public String onStart() {
        text = "/" + MessageUtils.getRandomAlphanumericString(Math.max(Bukkit.getOnlinePlayers().size() / 4, 15));

        return null;
    }

    @Override
    public String getDynamicDescription() {
        return String.format("&eSeja o mais rápido a digitar o comando &f%s&b.", text);
    }

    @Override
    public String getDynamicStatus() {
        return getDynamicDescription();
    }

    @Override
    protected boolean runTry(Player player, String parameter) {
        return parameter.equals(text);
    }
}
