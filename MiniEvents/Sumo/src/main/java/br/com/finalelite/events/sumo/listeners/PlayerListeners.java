package br.com.finalelite.events.sumo.listeners;


import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.events.sumo.SumoEvent;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

@RequiredArgsConstructor
public class PlayerListeners implements Listener {

    private final SumoEvent event;

    @EventHandler
    public void onMoveOut(PlayerMoveEvent event) {
        if (!this.event.isInGame())
            return;

        val player = event.getPlayer();

        if (this.event.getPlayerStatus(player) != EventPlayerStatus.ALIVE)
            return;

        if (this.event.isInArena(player))
            return;

        this.event.lose(player, "Você saiu da arena.");

        val damager = event.getPlayer().getLastDamageCause() == null ? null : event.getPlayer().getLastDamageCause().getEntity();
        if (damager == null || !(damager instanceof Player))
            return;

        val damagerPlayer = (Player) damager;

        if (this.event.getPlayerStatus(damagerPlayer) == EventPlayerStatus.ALIVE) {
            this.event.score(damagerPlayer, 1);
            MessageUtils.sendColouredMessage(damagerPlayer, String.format("&aVocê eliminou &f%s&a. Total: &f%d&a.",
                    player.getName(), this.event.getPlayerScore(damagerPlayer)));
        }
    }

}
