package br.com.finalelite.events.sumo;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.mini.BaseMiniEvent;
import br.com.finalelite.events.sumo.listeners.PlayerListeners;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;

public class SumoEvent extends BaseMiniEvent {
    public SumoEvent(JavaPlugin plugin) {
        super(plugin, "Sumo", "Seja o último a ficar em pé.", Material.STICK,
                EventReward.itemReward(new ItemStack(Material.EMERALD_BLOCK)),
                10 * 60, EventItemPolicy.GIVE_ITEMS, EventPlayerPolicy.getDefaultPlayerPolicy(),
                EventCommandsPolicy.onlyEventCommand(),
                EventLocations.getFromConfig(plugin.getConfig(), "Events.Sumo.Locations"));

        getMessages().setWillStartTimes(2);
        getMessages().setWillStartDelay(10);
        getMessages().setStopDelay(-1);
        getLocations().setTeleportToGameOnStart(false);

        val stick = new ItemStack(Material.STICK);
        stick.addUnsafeEnchantment(Enchantment.KNOCKBACK, 5);
        val meta = stick.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY + "Sumo");
        stick.setItemMeta(meta);
        setItems(Collections.singletonList(stick));

        plugin.getServer().getPluginManager().registerEvents(new PlayerListeners(this), plugin);
    }

    @Override
    public String getDynamicDescription() {
        return "&eSeja o último a ficar em pé.";
    }

    @Override
    public String getDynamicStatus() {
        return String.format("%s\n&eJogadores vivos: &f%s&e.", getDynamicDescription(), getAlivePlayers().size());
    }

    @Override
    protected boolean runTry(Player player, Object parameter) {
        return false;
    }

    @Override
    public void onLose(Player player, String reason) {
        if (getAlivePlayers().size() == 1) {
            end(new EventWinner<>(getAlivePlayers().get(0), getAlivePlayers().get(0).getName()));
        }
    }
}
