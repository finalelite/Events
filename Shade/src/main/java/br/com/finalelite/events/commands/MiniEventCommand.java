package br.com.finalelite.events.commands;

import br.com.finalelite.events.Main;
import br.com.finalelite.events.mini.MiniEventManager;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import lombok.val;

import java.util.Arrays;

public class MiniEventCommand extends BaseCommand {

    private final MiniEventManager eventManager;

    public MiniEventCommand(MiniEventManager eventManager, CommandablePlugin plugin) {
        super("votar");
        this.eventManager = eventManager;
        setAliases(Arrays.asList("votar"));

        playerListener = (cmd) -> {
            val player = cmd.getSender();
            val playerRole = PauloAPI.getInstance().getAccountsCache().getAccountInfo(player).getRole();

            if (playerRole == null) {
                cmd.reply("&cParece que você não está no banco de dados!");
                return true;
            }

            if (eventManager.isOpenToVote()) {
                eventManager.openVoteInventory(player);
                return true;
            } else {
                if (!playerRole.isMajorStaff())
                    return false;

                Main.getInstance().openVotes();

                return true;
            }
        };
    }
}
