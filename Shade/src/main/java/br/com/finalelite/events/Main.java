package br.com.finalelite.events;

import br.com.finalelite.events.base.EventManager;
import br.com.finalelite.events.combat.EventCombat;
import br.com.finalelite.events.commands.EventCommand;
import br.com.finalelite.events.commands.MiniEventCommand;
import br.com.finalelite.events.gladiator.EventGladiator;
import br.com.finalelite.events.lottery.LotteryEvent;
import br.com.finalelite.events.mini.MiniEventManager;
import br.com.finalelite.events.mining.MiningEvent;
import br.com.finalelite.events.parkour.ParkourEvent;
import br.com.finalelite.events.sumo.SumoEvent;
import br.com.finalelite.events.typing.TypingEvent;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import lombok.Getter;
import lombok.val;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;

public class Main extends CommandablePlugin {

    @Getter
    private static Main instance;
    @Getter
    private EventManager eventManager;
    @Getter
    private MiniEventManager miniEventManager;

    @Override
    public void onEnable() {
        instance = this;

        val configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists())
            saveDefaultConfig();

        eventManager = new EventManager();
        eventManager.register(new EventGladiator(this));
        eventManager.register(new EventCombat(this));

        miniEventManager = new MiniEventManager(this, eventManager);
        miniEventManager.registerEvent(new LotteryEvent(this));
        miniEventManager.registerEvent(new ParkourEvent(this));
        miniEventManager.registerEvent(new TypingEvent(this));
        miniEventManager.registerEvent(new MiningEvent(this));
        miniEventManager.registerEvent(new SumoEvent(this));

        registerCommand(new EventCommand());
        registerCommand(new MiniEventCommand(miniEventManager, this));
    }

    public boolean openVotes() {
        byte result = miniEventManager.startVote();
        if (result != 0) {
            return false;
        }

        Bukkit.broadcastMessage("\n");
        // TODO VIP
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&eUm evento automático será iniciado em 30 segundos. Escolha o evento usando &f/votar&e."));
        Bukkit.broadcastMessage("\n");


        new BukkitRunnable() {
            @Override
            public void run() {
                val votes = miniEventManager.endVote();
                if (votes == null) {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&cNenhum voto registrado, o evento foi cancelado."));
                    return;
                }
                var event = votes.getValue();
                if (votes.getKey() != 1) {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&eDeu empate. O evento " + event.getName() + " foi escolhido aleatóriamente."));
                }

                miniEventManager.startEvent(event);
            }
        }.runTaskLater(this, 20 * 20);

        return true;
    }
}
