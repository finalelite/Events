package br.com.finalelite.events.commands;

import br.com.finalelite.events.Main;
import br.com.finalelite.events.base.BaseEvent;
import br.com.finalelite.events.base.EventLocations;
import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.ExecutedCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import br.com.finalelite.pauloo27.api.config.ConfigUtils;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import lombok.val;

import java.util.Arrays;
import java.util.stream.Collectors;

public class EventCommand extends BaseCommand {
    public EventCommand() {
        super("evento");

        usage.addParameter(new Parameter("ação", DefaultParameterType.STRING, false));
        usage.addParameter(new Parameter("evento", DefaultParameterType.STRING, false));
        usage.addParameter(new Parameter("ação", DefaultParameterType.STRING, false));

        usage.setUsageMessageFormat("&cUso correto: /%%s %s. Se precisar de ajuda, use &f/evento ajuda&c.");

        playerListener = cmd -> {
            val player = cmd.getSender();
            val eventManager = Main.getInstance().getEventManager();
            val playerRole = PauloAPI.getInstance().getAccountsCache().getAccountInfo(player).getRole();

            if (playerRole == null) {
                cmd.reply("&cParece que você não está no banco de dados!");
                return true;
            }

            if (!cmd.hasArgument(0)) {
                val event = eventManager.getInGameOrWaitingEvents().size() != 0 ? eventManager.getInGameOrWaitingEvents().get(0) : null;

                if (event == null || event.isIdle()) {
                    cmd.reply("&cNenhum evento está acontecendo.");
                    if (playerRole.isMajorStaff())
                        cmd.reply("&aUse &f/" + cmd.getLabel() + " ajuda&a para ver como controlar os eventos.");
                    return true;
                }

                val playerStatus = event.getPlayerStatus(player);

                if (playerStatus == EventPlayerStatus.IDLE) {
                    if (event.isWaiting()) {
                        val result = event.join(player);
                        if (result != null)
                            cmd.reply("&cErro: " + result);
                    } else if (event.isInGame()) {
                        val result = event.joinSpectating(player);
                        if (result != null)
                            cmd.reply("&cErro: " + result);
                    }
                } else if (playerStatus.isSpectating()) {
                    val result = event.leaveSpectating(player);
                    if (result != null)
                        cmd.reply("&cErro: " + result);
                } else if (playerStatus == EventPlayerStatus.WAITING) {
                    val result = event.leave(player);
                    if (result != null)
                        cmd.reply("&cErro: " + result);
                } else if (playerStatus == EventPlayerStatus.ALIVE) {
                    cmd.reply("&cVocê não pode sair do evento.");
                }
            } else {
                val action = cmd.getArgumentAsString(0);
                if (action.equalsIgnoreCase("ver")) {
                    val event = eventManager.getInGameOrWaitingEvents().size() != 0 ? eventManager.getInGameOrWaitingEvents().get(0) : null;
                    if (event == null || event.isIdle()) {
                        cmd.reply("&cNenhum evento está acontecendo.");
                        if (playerRole.isMajorStaff())
                            cmd.reply("&aUse &f/" + cmd.getLabel() + " ajuda&a para ver como controlar os eventos.");
                        return true;
                    }

                    if (event.getLocations() == null || event.getLocations().getSpectating() == null) {
                        cmd.reply("&cEsse evento não suporta espectadores.");
                        return true;
                    }
                    
                    if(event.getPlayerStatus(player).isSpectating())
                        event.leaveSpectating(player);
                    else if(event.getPlayerStatus(player) == EventPlayerStatus.IDLE)
                        event.joinSpectating(player);
                } else if (action.equalsIgnoreCase("ajuda")) {
                    sendHelp(cmd);
                } else if (action.equalsIgnoreCase("lista")) {
                    sendEventList(cmd);
                } else if (action.equalsIgnoreCase("iniciar")) {
                    if (!cmd.hasArgument(1))
                        return false;

                    if (!playerRole.isMajorStaff()) {
                        cmd.reply("&cVocê não tem permissão pra usar esse comando.");
                        return true;
                    }

                    val eventName = cmd.getArgumentAsString(1);

                    val event = eventManager.getEvent(eventName) != null ? eventManager.getEvent(eventName) : null;

                    if (event == null) {
                        cmd.reply("&cEvento inválido.");
                        return false;
                    }

                    val result = event.start();
                    if (result != null)
                        cmd.reply("&cErro: " + result);
                } else if (action.equalsIgnoreCase("cancelar")) {
                    if (!playerRole.isMajorStaff()) {
                        cmd.reply("&cVocê não tem permissão pra usar esse comando.");
                        return true;
                    }

                    val event = eventManager.getInGameOrWaitingEvents().size() != 0 ? eventManager.getInGameOrWaitingEvents().get(0) : null;
                    if (event == null || event.isIdle()) {
                        cmd.reply("&cNenhum evento ocorrendo.");
                        return true;
                    }

                    event.cancel("Cancelado por " + player.getName());
                } else if (action.equalsIgnoreCase("warp")) {
                    if (!playerRole.isMajorStaff()) {
                        cmd.reply("&cVocê não tem permissão pra usar esse comando.");
                        return true;
                    }

                    if (!cmd.hasArgument(1) || !cmd.hasArgument(2))
                        return false;

                    val eventName = cmd.getArgumentAsString(1);
                    val warpName = cmd.getArgumentAsString(2);
                    val event = eventManager.getEvent(eventName) != null ? eventManager.getEvent(eventName) : null;
                    val defaultWarps = Arrays.asList("Spawn", "Game", "Spectating", "Waiting", "Pos1", "Pos2");

                    if (event == null) {
                        cmd.reply("&cEvento inválido.");
                        return false;
                    }

                    if (warpName.equalsIgnoreCase("lista")) {
                        cmd.reply("&eWarps padrão: &f" + String.join(", ", defaultWarps) + "&e.");
                        if (event.getLocations().getCustomLocations() == null)
                            cmd.reply("&eWarps especiais: Nenhuma.");
                        else
                            cmd.reply("&eWarps especiais: &f" + String.join(", ", event.getLocations().getCustomLocations().keySet()) + "&e.");
                        return true;
                    }

                    val location = player.getLocation();
                    if (!warpName.equalsIgnoreCase("reload") && !defaultWarps.contains(MessageUtils.capitalize(warpName)) && (event.getLocations().getCustomLocations() == null || !event.getLocations().getCustomLocations().keySet().contains(MessageUtils.capitalize(warpName)))) {
                        cmd.reply("&cNome de warp inválido. Use &f/evento warp " + eventName + " lista &cpara ver as warps válidas.");
                        return true;
                    }

                    if (!warpName.equalsIgnoreCase("reload")) {
                        Main.getInstance().getConfig().set("Events." + event.getName() + ".Locations." + MessageUtils.capitalize(warpName), ConfigUtils.serializeLocation(location));
                        Main.getInstance().saveConfig();
                        Main.getInstance().reloadConfig();
                        cmd.reply(String.format("&aLocalização &f%s &adefinida.", MessageUtils.capitalize(warpName)));
                    }

                    event.setLocations(EventLocations.getFromConfig(Main.getInstance().getConfig(), "Events." + event.getName() + ".Locations", event.getLocations().getCustomLocations() == null ? null : event.getLocations().getCustomLocations().keySet().stream().collect(Collectors.toList())));
                    cmd.reply("&aLocalizações recarregadas.");
                } else {
                    return false;
                }
            }
            return true;
        };
    }

    private void sendEventList(ExecutedCommand cmd) {
        cmd.reply("&aEventos: " + Main.getInstance().getEventManager().getEvents().stream().map(BaseEvent::getName).collect(Collectors.joining(", ")));
    }

    private void sendHelp(ExecutedCommand cmd) {
        sendFormattedMessage(cmd, "ver", "Espectar o evento");
        sendFormattedMessage(cmd, "iniciar <evento>", "Iniciar um evento");
        sendFormattedMessage(cmd, "lista", "Lista os eventos disponíveis");
        sendFormattedMessage(cmd, "cancelar", "Cancela o evento atual");
        sendFormattedMessage(cmd, "warp <evento> lista", "Lista as warps de um evento que precisam ser definidas.");
        sendFormattedMessage(cmd, "warp <evento> reload", "Recarrega as warps de um evento.");
        sendFormattedMessage(cmd, "warp <evento> <nome>", "Define uma warp para um evento.");
    }

    private void sendFormattedMessage(ExecutedCommand cmd, String subcommand, String message) {
        cmd.reply(String.format("&e */%s %s&f: %s.", cmd.getLabel(), subcommand, message));
    }
}
