package br.com.finalelite.events.gladiator;

import br.com.finalelite.events.base.*;
import br.com.finalelite.events.gladiator.commands.FEFaction;
import br.com.finalelite.events.gladiator.dabatase.SQLDatabase;
import br.com.finalelite.events.gladiator.listeners.PlayerListener;
import br.com.finalelite.pauloo27.api.commands.CommandManager;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import br.com.finalelite.pauloo27.api.utils.MapUtils;
import com.mrpowergamerbr.temmiewebhook.DiscordEmbed;
import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import com.mrpowergamerbr.temmiewebhook.TemmieWebhook;
import com.mrpowergamerbr.temmiewebhook.embed.AuthorEmbed;
import com.mrpowergamerbr.temmiewebhook.embed.FieldEmbed;
import com.mrpowergamerbr.temmiewebhook.embed.FooterEmbed;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class EventGladiator extends BaseEvent {

    private static Map<String, String> emojis = new HashMap<>();

    static {
        emojis.put("1", ":first_place:");
        emojis.put("2", ":second_place:");
        emojis.put("3", ":third_place:");
    }

    @Getter
    private SQLDatabase db;
    private Map<Player, String> factions = new HashMap<>();
    @Getter
    private int factionsCountOnStart;

    public EventGladiator(JavaPlugin plugin) {
        super(plugin, "Gladiador", "Mata-mata de esquadrões", new EventReward("1 sinalizador", winner -> Bukkit.broadcastMessage("O esquadrão " + winner.getName() + " merece um sinalizador!")),
                60,
                EventItemPolicy.PLAYER_ITEMS, new EventPlayerPolicy(2, -1), EventCommandsPolicy.onlyEventCommand(), EventLocations.getFromConfig(plugin.getConfig(), "Events.Gladiador.Locations"), getEventMessages());
        plugin.getServer().getPluginManager().registerEvents(new PlayerListener(this), plugin);

        connectToDB();

        // TODO remove
        if (plugin instanceof CommandablePlugin) {
            CommandManager.register(new FEFaction(this), (CommandablePlugin) plugin);
        } else {
            System.out.println("Cannot register the command");
        }

        addPlaceHolder("players_size_by_factions", this::getFactionsStatus);
        addPlaceHolder("factions_waiting", () -> MessageUtils.pluralize(getWaitingFactions().size(), "&eesquadrão", "&eesquadrões"));
        addPlaceHolder("factions_alive", () -> MessageUtils.pluralize(getAliveFactions().size(), "&eesquadrão", "&eesquadrões"));
    }

    private static EventMessages getEventMessages() {
        return EventMessages.builder()
                .willStart(
                        Arrays.asList("", "&eEvento &f${event_name} &eirá iniciar em &f${seconds_to_start} segundos&e.",
                                "&f${players_waiting} e &f${factions_waiting} já estão no evento.", ""))
                .willStartDelay(5)
                .willStartTimes(5)
                .started(Arrays.asList("", "&eEvento &f${event_name} &einiciado com &f${players_alive} e &f${factions_alive}.", ""))
                .completeStatus(Arrays.asList("", "&eEsquadrões: &f${factions_alive}&e.", "&eJogadores: &a${players_size_by_factions}", ""))
                .timeout(Arrays.asList("", "&eO tempo máximo do evento foi exedido. O esquadrão com mais jogadores vivos será o vencedor. Em caso de empate, os abates serão usados.", ""))
                .status(Arrays.asList("", "&eO evento &f${event_name} &ejá está acontecendo a &f${event_time}&e.",
                        "&eJogadores: &f${players_alive}&e.", "&eEsquadrões: &f${factions_alive}&e.", ""))
                .statusDelay(60)
                .build();
    }

    private void connectToDB() {
        db = new SQLDatabase();
    }

    @Override
    protected EventWinner onTimeMax() {
        val topPlayer = getTopFactions(1).entrySet().stream().findFirst().get().getKey();
        return new EventWinner<>(topPlayer, topPlayer);
    }

    public List<String> getFactions() {
        return factions.values().stream().distinct().collect(Collectors.toList());
    }

    public List<Player> getPlayersByFaction(String faction) {
        return factions.entrySet().stream().filter(entry -> entry.getValue().equals(faction)).map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public Map<String, Integer> getTopFactions(int limit) {
        return MapUtils.sortMap(factions.values().stream().distinct()
                .map(faction -> new AbstractMap.SimpleEntry<>(faction, getFactionScore(faction)))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue)), limit);
    }

//    public Map<String, Integer> getTopFactionsByPlayerCount(int limit) {
//
//    }

    public Map<String, Integer> getPlayersByFaction() {
        return factions.values().stream().distinct().map(s -> new AbstractMap.SimpleEntry<>(s, getPlayersByFaction(s).size()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public Map<String, Integer> getAlivePlayersByFaction() {
        return factions.entrySet().stream().filter(entry -> getPlayerStatus(entry.getKey()) == EventPlayerStatus.ALIVE)
                .map(Map.Entry::getValue)
                .distinct().map(s -> new AbstractMap.SimpleEntry<>(s, getPlayersByFaction(s).size()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public List<Player> getAlivePlayersByFaction(String faction) {
        return factions.entrySet().stream()
                .filter(entry -> entry.getValue().equalsIgnoreCase(faction) && getPlayerStatus(entry.getKey()) == EventPlayerStatus.ALIVE)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<String> getAliveFactions() {
        return factions.entrySet().stream().filter(entry -> getPlayerStatus(entry.getKey()) == EventPlayerStatus.ALIVE)
                .map(Map.Entry::getValue)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> getWaitingFactions() {
        return factions.entrySet().stream().filter(entry -> getPlayerStatus(entry.getKey()) == EventPlayerStatus.WAITING)
                .map(Map.Entry::getValue)
                .distinct()
                .collect(Collectors.toList());
    }

    public int getFactionScore(String factionName) {
        return factions.entrySet().stream()
                .filter(entry -> entry.getValue().equalsIgnoreCase(factionName))
                .map(Map.Entry::getKey)
                .map(this::getPlayerScore).mapToInt(score -> score).sum();
    }

    public String getFactionsStatus() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        getAlivePlayersByFaction().forEach((faction, players) -> {
            sb.append(faction).append(": ").append(getFactionScore(faction)).append(" (").append(players).append(" jogadores)").append("\n");
        });
        return sb.toString();
    }

    public void sendDiscordMessage(EventWinner winner) {
        String url = getPlugin().getConfig().getString("Events.Gladiador.Webhook");
        if (url == null) {
            getPlugin().getLogger().warning("Cannot send webhook message, invalid URL. " +
                    "Sets the URL in the Config file at 'Events.Gladiador.Webhook'.");
            return;
        }
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        AtomicInteger position = new AtomicInteger();
        TemmieWebhook temmie = new TemmieWebhook(url);

        val topFactions = getTopFactions(3);
        StringBuilder topFactionsFormatted = new StringBuilder();
        topFactions.forEach((fac, score) -> topFactionsFormatted.append(emojis.get(String.valueOf(position.incrementAndGet()))).append(" **").append(fac).append("** - ").append(score).append(" abates").append("\n"));
        if (topFactionsFormatted.toString().isEmpty())
            topFactionsFormatted.append("Ninguém");

        position.set(0);
        val topPlayers = getTopPlayers(3);
        StringBuilder topPlayersFormatted = new StringBuilder();
        topPlayers.forEach((player, score) -> topPlayersFormatted.append(emojis.get(String.valueOf(position.incrementAndGet()))).append("**").append(player.getName()).append("** - ").append(score).append(" abates").append("\n"));
        if (topPlayersFormatted.toString().isEmpty())
            topPlayersFormatted.append("Ninguém");

        DiscordEmbed de = DiscordEmbed.builder()
                .title("Evento Gladiador")
                .description("\nResultado do evento Gladiador\n")
                .color(0x00c1ff)
                .author(AuthorEmbed.builder()
                        .icon_url("https://i.imgur.com/JYFM8t9.png")
                        .name("Final Elite")
                        .url("https://finalelite.com.br")
                        .build())
                .footer(FooterEmbed.builder()
                        .icon_url("https://i.imgur.com/JYFM8t9.png")
                        .text("Evento realizado dia " + df.format(this.getRealStarTime()))
                        .build())
                .fields(Arrays.asList(
                        FieldEmbed.builder()
                                .name("\nParticipação")
                                .value(String.format(":family_mwbb: **%d esquadrões**\n:walking: **%d jogadores**\n", getFactionsCountOnStart(), getPlayersCountOnStart()))
                                .build(),
                        FieldEmbed.builder()
                                .name("\nEsquadrão vencedor")
                                .value(String.format(":medal: **%s**", winner.getName()))
                                .build(),
                        FieldEmbed.builder()
                                .name("\nEsquadrões com mais abates")
                                .value(topFactionsFormatted.toString())
                                .build(),
                        FieldEmbed.builder()
                                .name("\nJogadores com mais abates")
                                .value(topPlayersFormatted.toString())
                                .build()
                ))
                .build();

        DiscordMessage dm = DiscordMessage.builder()
                .username("Final Elite")
                .content("")
                .avatarUrl("https://i.imgur.com/JYFM8t9.png")
                .embeds(Collections.singletonList(de))
                .build();

        temmie.sendMessage(dm);
    }

    @Override
    public void onEnd(EventWinner winner) {
        sendDiscordMessage(winner);
    }

    @Override
    public void onLose(Player player, String reason) {
        val faction = factions.get(player);
        val messages = Arrays.asList("", "&eO jogador &f" + player.getName() + " &edo esquadrão &f"
                        + faction + " &efoi eliminado.",
                (getAlivePlayersByFaction(faction).size() == 0 ?
                        "&eCom isso, o esquadrão &f" + faction + " &eestá fora do evento" :
                        "&eCom isso, o esquadrão &f" + faction + " &etem mais &f" + getAlivePlayersByFaction(faction).size() +
                                (getAlivePlayersByFaction(faction).size() == 1 ? " &ejogador vivo." : " &ejogadores vivos.")), "");
        announce(messages);

        if (getAliveFactions().size() == 1)
            end(new EventWinner<>(getAliveFactions().get(0), getAliveFactions().get(0)));
    }

    @Override
    public void onStop() {
        factions.clear();
    }

    @Override
    public String onJoin(Player player) {
        val fac = db.getFacName(player);
        if (fac == null)
            return "&cVocê não está em um esquadrão.";

        factions.put(player, fac);
        return null;
    }

    @Override
    public String onStart() {
        if (getFactions().size() < 2) {
            return "&cEsquadrões insuficientes.";
        }

        factionsCountOnStart = getFactions().size();
        return null;
    }

    @Override
    public String onLeave(Player player) {
        factions.remove(player);
        return null;
    }

}
