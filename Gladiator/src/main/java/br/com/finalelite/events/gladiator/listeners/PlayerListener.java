package br.com.finalelite.events.gladiator.listeners;

import br.com.finalelite.events.base.EventPlayerStatus;
import br.com.finalelite.events.gladiator.EventGladiator;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@AllArgsConstructor
public class PlayerListener implements Listener {
    private final EventGladiator gladiator;

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        val player = event.getEntity();
        val killer = player.getKiller();

        if (gladiator.isIdle() || gladiator.isWaiting()) {
            return;
        }

        if (gladiator.getPlayerStatus(player) == EventPlayerStatus.ALIVE) {
            if (killer != null && gladiator.getPlayerStatus(killer) == EventPlayerStatus.ALIVE)
                gladiator.score(killer, 1);

            gladiator.lose(player, (killer == null ? "Voce morreu." : "Morto por " + killer.getName() + "."));
        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        gladiator.getDb().removePlayerFromCache(event.getPlayer());
    }

}
