package br.com.finalelite.events.gladiator.commands;

import br.com.finalelite.events.gladiator.EventGladiator;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import lombok.var;

public class FEFaction extends BaseCommand {
    public FEFaction(EventGladiator event) {
        super("fefaction");
        setPermission("adm.admin.admir");
        usage.addParameter(new Parameter("new/set", DefaultParameterType.STRING, true));
        usage.addParameter(new Parameter("nome", DefaultParameterType.STRING, true));
        usage.addParameter(new Parameter("cargo", DefaultParameterType.STRING, true));

        playerListener = (cmd) -> {
            val player = cmd.getSender();

            if (!player.isOp())
                return false;
            val action = (String) cmd.getArgument(0);
            var create = true;
            if (action.equalsIgnoreCase("set"))
                create = false;
            else if (!action.equalsIgnoreCase("new"))
                return false;
            val name = (String) cmd.getArgument(1);
            val role = (String) cmd.getArgument(2);

            if (create)
                event.getDb().createFaction(player, name, role);
            else
                event.getDb().setFaction(player, name, role);
            return true;
        };
    }
}
