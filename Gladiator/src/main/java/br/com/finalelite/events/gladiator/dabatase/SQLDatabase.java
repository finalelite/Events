package br.com.finalelite.events.gladiator.dabatase;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.gitlab.pauloo27.core.sql.EzInsert;
import com.gitlab.pauloo27.core.sql.EzSQL;
import com.gitlab.pauloo27.core.sql.EzUpdate;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SQLDatabase {

    private EzSQL sql;
    private Map<String, String> playerUUID = new HashMap<>();

    public SQLDatabase() {
        // TODO remove tries from constructor :P
        sql = PauloAPI.getInstance().getEzSQL("mixedup");
        try {
            sql.connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public String getPlayerUUID(Player player) {
        if (playerUUID.containsKey(player.getName()))
            return playerUUID.get(player.getName());

        playerUUID.put(player.getName(), getUUIDFromDB(player.getName()));
        return getPlayerUUID(player);
    }

    public void removePlayerFromCache(Player player) {
        playerUUID.remove(player);
    }

    public Player getPlayerByUUID(String uuid) {
        return playerUUID.entrySet().stream().filter(entry -> entry.getValue().equals(uuid)).map(entry -> Bukkit.getPlayer(entry.getKey()))
                .findFirst().orElse(getPlayerFromDB(uuid));
    }

    public Player getPlayerFromDB(String uuid) {
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT Nick FROM hubfinal.Account_data WHERE uuid = ?;");
            st.setString(1, uuid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return Bukkit.getPlayer(rs.getString("Nick"));
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUUIDFromDB(String nick) {
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT uuid FROM hubfinal.Account_data WHERE Nick = ?;");
            st.setString(1, nick);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("UUID");
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFacName(Player player) {
        val uuid = getPlayerUUID(player);
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT * FROM PlayerFac_data WHERE UUID = ?");
            st.setString(1, uuid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Nome");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setFaction(Player player, String name, String role) {
        try {
            sql.getTable("PlayerFac_data")
                    .update(new EzUpdate().set("Nome", name).set("Hierarquia", role)
                            .where().equals("UUID", getPlayerUUID(player))).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createFaction(Player player, String name, String role) {
        try {
            sql.getTable("PlayerFac_data")
                    .insert(new EzInsert("UUID, nome, Hierarquia", getPlayerUUID(player), name, role)).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
